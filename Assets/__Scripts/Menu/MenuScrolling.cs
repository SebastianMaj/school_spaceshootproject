﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScrolling : MonoBehaviour
{
	// Variables
	public RawImage scrollingImage;
	private float _scrollSpeed = 0.2f;
	private float _previousSpeed;
	// Offset for the image texture
	private Vector2 _currentOffset = Vector2.zero;
	
	// Update called once per frame
	private void Update()
	{
		_currentOffset.x += _scrollSpeed * Time.deltaTime;
		scrollingImage.material.SetTextureOffset("_MainTex", _currentOffset);
	}

	// Set the speed of the scrolling background
	public void SetSpeed(float newSpeed, float timer = 0f)
	{
		_previousSpeed = _scrollSpeed;
		_scrollSpeed = newSpeed;

		if (timer > 0f)
		{
			StartCoroutine(ResetTimer(timer));
		}
	}

	// Reset the speed back to the previous speed after the time specified passes
	private IEnumerator ResetTimer(float time)
	{
		yield return new WaitForSeconds(time);
		_scrollSpeed = _previousSpeed;
	}
}
