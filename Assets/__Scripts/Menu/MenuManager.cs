﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
	// Current Menu
	private MenuState _currentState;
	public List<MenuState> menuStates = new List<MenuState>();
	
	[Space]
	
	// Menu Scrolling
	public MenuScrolling menuScrolling;

	// Awake
	private void Awake()
	{
		// Initial Menu State
		_currentState = menuStates[0];
		
		// Initiate Volume Scales
		if (VolumeScales.Count == 0)
		{
			for (int i = 0; i < 4; i++)
			{
				VolumeScales.Add(i, 1);
			}
		}
		AudioDropDownChanged(0);
		
		// Initiate the Background static list
		if (Backgrounds.Count == 0)
		{
			for (int i = 0; i < backgroundInspector.Count; i++)
			{
				Backgrounds.Add(i, backgroundInspector[i]);
			}
		}
		
		// Initiate the enemy colors and points
		if (EnemyColors.Count == 0)
		{
			for (int i = 0; i < 5; i++)
			{
				EnemyColors.Add(i, 0);
				EnemyPoints.Add(i, 0);
			}
		}

		StartCoroutine(PlayMusic());
		
		// Initiate Levels
		InitiateLevels();
	}
	
	/**
	 * 	MENU MANAGEMENT
	 */
	public void StartGame()
	{
		StopCoroutine(PlayMusic());
		SceneManager.LoadScene("Game");
	}
	
	// Quits the game
	public void ExitGame()
	{
		print("Exiting game");
        
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
	
	public void OpenMenuState(string newStateName)
	{
		// Get the state struct object
		var newState = menuStates.FirstOrDefault(x => x.StateName == newStateName);
		if (!newState.StateObject)
			return;
		
		// Animate to new state
		menuScrolling.SetSpeed(1.5f, 1.0f);
		_currentState.StateObject.DOMoveX(-1000, 1.0f);
		newState.StateObject.position = new Vector3(1000, newState.StateObject.position.y, newState.StateObject.position.z);
		newState.StateObject.gameObject.SetActive(true);
		newState.StateObject.DOMoveX(0, 1.0f);
		_currentState = newState;
	}
	
	/**
	 * 	LEVELS
	 */
	
	[Header("Levels")]
	
	public TextMeshProUGUI titleText;
	public TMP_InputField enemyNumInput;
	public Toggle[] enemyToggles = new Toggle[]{};
	public TMP_InputField scoreToWinInput;
	
	// Levels
	private int _currentLevelEdit = 0;
	public static List<Level> levels = new List<Level>();
	private void InitiateLevels()
	{
		Level bronze = NewLevel("Bronze", 1);
		Level silver = NewLevel("Silver", 2);
		Level gold = NewLevel("Gold", 3);
		
		levels.Add(bronze);
		levels.Add(silver);
		levels.Add(gold);
	}
	private Level NewLevel(string levelName, int index)
	{
		Level level = new Level();
		level.LevelName = levelName;
		level.maxEnemies = 2 * index;
		level.reqScore = 100 * index;

		for (int i = 0; i < 5; i++)
		{
			level.enemiesToggled.Add(true);
		}
		
		return level;
	}

	// Select the level to edit
	public void SelectLevelEdit(int index)
	{
		_currentLevelEdit = index;
		Level level = levels[index];

		titleText.text = "LEVEL: " + level.LevelName;
		enemyNumInput.text = level.maxEnemies.ToString();
		for (int i = 0; i < 5; i++)
		{
			enemyToggles[i].isOn = level.enemiesToggled[i];
		}
		scoreToWinInput.text = level.reqScore.ToString();
		OpenMenuState("STATE_INNERLEVEL");
	}
	
	// Modify level num enemies on screen
	public void UpdateScreenEnemies()
	{
		levels[_currentLevelEdit].maxEnemies = int.Parse(enemyNumInput.text);
	}
	
	// Modify the toggle
	public void UpdateEnemyToggles()
	{
		Level level = levels[_currentLevelEdit];

		for (int i = 0; i < 5; i++)
		{
			level.enemiesToggled[i] = enemyToggles[i].isOn;
		}
	}
	
	// Modify the score to win
	public void UpdateScoreToWin()
	{
		Level level = levels[_currentLevelEdit];
		level.reqScore = int.Parse(scoreToWinInput.text);
	}
	
	private IEnumerator PlayMusic()
	{
		while (AudioManager.instance == null) yield return null;
		
		while (true)
		{
			AudioManager.instance.PlaySound("MUSIC_BG");
			yield return new WaitForSeconds(13f);
		}
	}
	
	/**
	 * 	ENEMY
	 */
	[Header("Enemies")]
	
	// Color options
	public static Color[] ColorOptions = new[]
	{
		Color.grey, Color.green, Color.blue, Color.red, Color.yellow, new Color(255, 165, 0), new Color(128, 0, 128),
		Color.white
	};
	public static Dictionary<int,int> EnemyColors = new Dictionary<int, int>();
	
	// Point options
	public static int[] PointOptions = new[] { 5, 10, 15, 20, 25 };
	public static Dictionary<int, int> EnemyPoints = new Dictionary<int, int>();

	// Color dropdown
	public void ChooseNewColorEnemy0(Int32 colorIndex)
	{
		EnemyColors[0] = colorIndex;
	}
	public void ChooseNewColorEnemy1(Int32 colorIndex)
	{
		EnemyColors[1] = colorIndex;
	}
	public void ChooseNewColorEnemy2(Int32 colorIndex)
	{
		EnemyColors[2] = colorIndex;
	}
	public void ChooseNewColorEnemy3(Int32 colorIndex)
	{
		EnemyColors[3] = colorIndex;
	}
	public void ChooseNewColorEnemy4(Int32 colorIndex)
	{
		EnemyColors[4] = colorIndex;
	}
	
	// Point dropdown
	public void ChooseNewPointEnemy0(Int32 pointIndex)
	{
		EnemyPoints[0] = pointIndex;
	}
	public void ChooseNewPointEnemy1(Int32 pointIndex)
	{
		EnemyPoints[1] = pointIndex;
	}
	public void ChooseNewPointEnemy2(Int32 pointIndex)
	{
		EnemyPoints[2] = pointIndex;
	}
	public void ChooseNewPointEnemy3(Int32 pointIndex)
	{
		EnemyPoints[3] = pointIndex;
	}
	public void ChooseNewPointEnemy4(Int32 pointIndex)
	{
		EnemyPoints[4] = pointIndex;
	}
		
	/**
	 * 	AUDIO
	 */
	public static Dictionary<int, float> VolumeScales = new Dictionary<int, float>();
	[Header("Audio")]
	public Slider volumeSlider;
	public TextMeshProUGUI volumeText;
	private Int32 _currentDropOption = 0;
	
	public void AudioDropDownChanged(Int32 dropOption)
	{
		_currentDropOption = dropOption;
		volumeSlider.value = VolumeScales[dropOption];
		volumeText.text = $"Volume: {Mathf.Round((VolumeScales[dropOption]/1)*100)}";
	}

	public void UpdateVolumeSlider()
	{
		var sliderValue = volumeSlider.value;
		VolumeScales[_currentDropOption] = sliderValue;
		volumeText.text = $"Volume: {Mathf.Round((sliderValue/1)*100)}";
	}
	
	/**
	 * 	BACKGROUND
	 */
	public static Dictionary<int, Sprite> Backgrounds = new Dictionary<int, Sprite>();
	public static int CurrentBackground = 0;
	[Header("Background")]
	public List<Sprite> backgroundInspector = new List<Sprite>();
	public Image backgroundPreviewImage;

	public void BackgroundDropDownChanged(Int32 dropOption)
	{
		CurrentBackground = dropOption;
		backgroundPreviewImage.sprite = Backgrounds[dropOption];
	}
}

[Serializable]
public struct MenuState
{
	[CanBeNull] public string StateName;
	[CanBeNull] public Transform StateObject;
}

[Serializable]
public class Level
{
	public string LevelName;
	public int maxEnemies;
	public List<bool> enemiesToggled = new List<bool>();
	public int reqScore;
}
