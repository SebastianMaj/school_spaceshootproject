﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
	public static AudioManager instance;
	
	// List of audio files
	public List<AudioFile> audioFiles = new List<AudioFile>();
	
	// Audio Sources
	public AudioSource musicSource;
	public AudioSource soundSource;

	// Awake
	private void Awake()
	{
		// Keep only a single instance
		if (instance != null && instance != this)
		{
			Destroy(gameObject);
		}

		instance = this;
		DontDestroyOnLoad(gameObject);
	}

	// Play a sound by it's ID
	public void PlaySound(string soundID)
	{
		var sound = audioFiles.FirstOrDefault(x => x.audioName == soundID);
		if (sound.audioClip == null)
		{
			print("Failed to find " + soundID);
			return;
		}

		if(sound.audioChannelID <= 2)
			musicSource.PlayOneShot(sound.audioClip, MenuManager.VolumeScales[sound.audioChannelID]);
		else
			soundSource.PlayOneShot(sound.audioClip, MenuManager.VolumeScales[sound.audioChannelID]);
	}
}

[Serializable]
public struct AudioFile
{
	public string audioName;
	public int audioChannelID;
	[CanBeNull] public AudioClip audioClip;
}
