﻿using System.Collections; // Required for Arrays & other Collections
using System.Collections.Generic; // Required for Lists and Dictionaries
using UnityEngine; // Required for Unity

public class Enemy : MonoBehaviour 
{
	[Header("Set in Inspector: Enemy")]
	public int enemyIndex = 0;
	public float speed = 10f; // The speed in m/s
	public float fireRate = 0.3f; // Seconds/shot (Unused)
	public float health = 10;
	public int score = 100; // Points earned for destroying this
	public float showDamageDuration = 0.1f; // # seconds to show damage
	public float powerUpDropChance = 1f;
	public GameObject projectilePrefab;

	[Header("Set Dynamically: Enemy")]
	public Color[] originalColors;
	public Material[] materials;// All the Materials of this & its children
	public bool showingDamage = false;
	public float damageDoneTime; // Time to stop showing damage
	public bool notifiedOfDestruction = false; // Will be used later
	
	protected BoundsCheck boundsCheck;

	// This is a Property: A method that acts like a field
	public Vector3 pos { 
		get {
			return( this.transform.position );
		} set {
			this.transform.position = value;
		}
	}

	void Awake() 
	{
		boundsCheck = GetComponent<BoundsCheck>();
		
		// Get materials and colors for this GameObject and its children
		materials = Utils.GetAllMaterials( gameObject );
		originalColors = new Color[materials.Length];
		for (int i=0; i<materials.Length; i++)
		{
			materials[i].color = MenuManager.ColorOptions[MenuManager.EnemyColors[enemyIndex]];
			originalColors[i] = materials[i].color;
		}
	}

	void Update() 
	{
		Move();

		if ( showingDamage && Time.time > damageDoneTime ) 
		{
			UnShowDamage();
		}

		if ( boundsCheck != null && boundsCheck.offDown ) {
			// We're off the bottom, so destroy this GameObject
			Destroy( gameObject );
		}
	}

	public virtual void Move() 
	{
		Vector3 tempPos = pos;
		tempPos.y -= speed * Time.deltaTime;
		pos = tempPos;
	}

	void OnCollisionEnter( Collision coll ) 
	{
		GameObject otherGO = coll.gameObject;
		switch (otherGO.tag) 
		{
			case "ProjectilePlayer":
				Projectile p = otherGO.GetComponent<Projectile>();
				
				ShowDamage();
				
				if (!boundsCheck.isOnScreen) 
				{
					Destroy( otherGO );
					break;
				}

				health -= Main.GetWeaponDefinition(p.type).damageOnHit;
				if (health <= 0) 
				{ 
					AudioManager.instance.PlaySound("EXPLODE_ENEMY");
					
					if(!notifiedOfDestruction)
					{
						Main.S.shipDestroyed( this );
					}
					notifiedOfDestruction = true;
					
					// Destroy this Enemy
					Destroy(this.gameObject);
				}
				else
				{
					MakeProjectile();
				}
				Destroy( otherGO );
				break;
			default:
				print( "Enemy hit by non-ProjectilePlayer: " + otherGO.name ); // f
				break;
		}
	}

	void MakeProjectile()
	{
		GameObject go = Instantiate<GameObject>( projectilePrefab );
		go.tag = "ProjectileEnemy";
		go.layer = LayerMask.NameToLayer("ProjectileEnemy");
		go.transform.position = transform.position;
		Projectile p = go.GetComponent<Projectile>();
		p.rigid.velocity = new Vector2(0, -50);
		p.type = WeaponType.enemy;
	}

	void ShowDamage() 
	{
		foreach (Material m in materials) 
		{
			m.color = Color.red;
		} 
		showingDamage = true;
		damageDoneTime = Time.time + showDamageDuration;
	}
		
	void UnShowDamage() 
	{
		for ( int i=0; i<materials.Length; i++ ) 
		{
			materials[i].color = originalColors[i];
		} 
		showingDamage = false;
	}
}