﻿using System.Collections;
using System.Collections.Generic; // Required to use Lists or Dictionaries
using UnityEngine; // Required for Unity
using UnityEngine.SceneManagement; // For loading & reloading of scenes
using TMPro;
using System.Diagnostics;
using System.Linq;

public class Main : MonoBehaviour 
{
	static public Main S; // A singleton for Main
	static Dictionary<WeaponType, WeaponDefinition> WEAP_DICT;

	public Renderer bgRenderer;

	// Timer
	public TextMeshProUGUI timerText;
	public TextMeshProUGUI pointsText;
	public TextMeshProUGUI levelText;
	// Values
	private int _currentLevelIndex = 0;
	private Level _currentLevel;
	private int _currentPoints = 0;
	public int currentPoints
	{
		get { return _currentPoints; }
		set { _currentPoints = value;
			if (value >= _currentLevel.reqScore) nextLevel();
		}
	}
	private Stopwatch _currentStopwatch = new Stopwatch();
	
	[Header("Set in Inspector")]
	public GameObject[] prefabEnemies; // Array of Enemy prefabs
	public float enemySpawnPerSecond = 0.5f; // # Enemies/second
	public float enemyDefaultPadding = 1.5f; // Padding for position
	public WeaponDefinition[] weaponDefinitions;
	public GameObject prefabPowerUp;
	public WeaponType[] powerUpFrequency = new WeaponType[] { 
		WeaponType.blaster, WeaponType.blaster, 
		WeaponType.spread, WeaponType.shield };

	private BoundsCheck bndCheck;

	public int spawnedEnemies = 0;

	public void shipDestroyed( Enemy e ) {
		if(Random.value <= e.powerUpDropChance) {
			int ndx = Random.Range(0, powerUpFrequency.Length);
			WeaponType puType = powerUpFrequency[ndx];
			
			GameObject go = Instantiate( prefabPowerUp ) as GameObject;
			PowerUp pu = go.GetComponent<PowerUp>();
			pu.SetType(puType);
			pu.transform.position = e.transform.position;
		}
		
		Main.S.currentPoints += MenuManager.PointOptions[e.enemyIndex];
	}

	void Awake() 
	{
		S = this;
		// Set bndCheck to reference the BoundsCheck component on this
		bndCheck = GetComponent<BoundsCheck>();
		// Invoke SpawnEnemy() once (in 2 seconds, based on default values)
		Invoke( "SpawnEnemy", 1f/enemySpawnPerSecond );

		WEAP_DICT = new Dictionary<WeaponType, WeaponDefinition>();
		foreach(WeaponDefinition def in weaponDefinitions) {
			WEAP_DICT[def.type] = def;
		}

		_currentLevelIndex = 0;
		_currentLevel = MenuManager.levels[_currentLevelIndex];
		currentPoints = 0;
		_currentStopwatch = new Stopwatch();
		_currentStopwatch.Start();
		
		bgRenderer.sharedMaterial.SetTexture("_MainTex", MenuManager.Backgrounds[MenuManager.CurrentBackground].texture);

		StartCoroutine(PlayMusic());
	}

	private void Update()
	{
		// Set GUI
		timerText.text = _currentStopwatch.Elapsed.Seconds.ToString("00") + "." + _currentStopwatch.Elapsed.Milliseconds.ToString("00");
		pointsText.text = "Score: " + currentPoints;
		levelText.text = _currentLevel.LevelName;
	}

	private IEnumerator PlayMusic()
	{
		while (true)
		{
			AudioManager.instance.PlaySound("MUSIC_BG");
			yield return new WaitForSeconds(13f);
		}
	}

	private void nextLevel()
	{
		if (_currentLevelIndex + 1 < MenuManager.levels.Count)
		{
			_currentLevelIndex++;
			_currentLevel = MenuManager.levels[_currentLevelIndex];
		}
		else
		{
			Restart();
		}
	}

	public void PauseGame()
	{
		Time.timeScale = 0;
		_currentStopwatch.Stop();
	}

	public void UnPauseGame()
	{
		Time.timeScale = 1;
		_currentStopwatch.Start();
	}

	public void SpawnEnemy()
	{
		// Check if room to spawn
		if (spawnedEnemies >= _currentLevel.maxEnemies)
		{
			// Invoke SpawnEnemy() again
			Invoke( "SpawnEnemy", 1f/enemySpawnPerSecond );
			return;
		}

		// Pick a random Enemy prefab to instantiate
		int ndx = Random.Range(0, prefabEnemies.Length);
		GameObject go = Instantiate<GameObject>( prefabEnemies[ ndx ] );

		if (!_currentLevel.enemiesToggled[go.GetComponent<Enemy>().enemyIndex])
		{
			Destroy(go);
		}
		else
		{
			// Position the Enemy above the screen with a random x position
			float enemyPadding = enemyDefaultPadding; 
		
			if (go.GetComponent<BoundsCheck>() != null) 
			{
				enemyPadding = Mathf.Abs( go.GetComponent<BoundsCheck>().radius );
			}

			// Set the initial position for the spawned Enemy 
			Vector3 pos = Vector3.zero;
			float xMin = -bndCheck.camWidth + enemyPadding;
			float xMax = bndCheck.camWidth - enemyPadding;
			pos.x = Random.Range( xMin, xMax );
			pos.y = bndCheck.camHeight + enemyPadding;
			go.transform.position = pos;
		}
		
		// Invoke SpawnEnemy() again
		Invoke( "SpawnEnemy", 1f/enemySpawnPerSecond );
	}

	public void DelayedRestart(float delay) {
		AudioManager.instance.PlaySound("MUSIC_LOSE");
		Invoke("Restart", delay);
	}

	public void RestartGame()
	{
		SceneManager.LoadScene("Game");
	}

	public void Restart() {
		SceneManager.LoadScene("Menu");
	}

	/// Static function that gets a WeaponDefinition from the WEAP_DICT static
	/// protected field of the Main class.
	/// </summary>
	/// <returns>The WeaponDefinition or, if there is no WeaponDefinition with
	/// the WeaponType passed in, returns a new WeaponDefinition with a
	/// WeaponType of none..</returns>
	/// <param name="wt">The WeaponType of the desired WeaponDefinition</param>
	static public WeaponDefinition GetWeaponDefinition( WeaponType wt ) 
	{ 
		// Check to make sure that the key exists in the Dictionary
		// Attempting to retrieve a key that didn't exist, would throw an error,
		// so the following if statement is important.
		if (WEAP_DICT.ContainsKey(wt)) 
		{
			return( WEAP_DICT[wt] );
		}
		// This returns a new WeaponDefinition with a type of WeaponType.none,
		// which means it has failed to find the right WeaponDefinition
		return(new WeaponDefinition());
	}
}