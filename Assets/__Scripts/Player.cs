﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class Player : MonoBehaviour 
{
	static public Player S; // Singleton

	[Header("Set in Inspector")]	
	// These fields control the movement of the ship
	public float speed = 30;
	public float rollMult = -45;
	public float pitchMult = 30;
	public float gameRestartDelay = 2f;
	public GameObject projectilePrefab;
	public float projectileSpeed = 40;
	public Weapon[] weapons;
	
	[Header("Set Dynamically")]
	private float _shieldLevel = 1;
	private GameObject lastTriggerGo = null;
	public delegate void WeaponFireDelegate();
	public WeaponFireDelegate fireDelegate;

	void Start() 
	{
		if (S == null) {
			S = this; // Set the Singleton
		} else {
			Debug.LogError("Player.Awake() - Attempted to assign second Player.S!");
		}

		//fireDelegate += TempFire;

		ClearWeapons();
		weapons[0].SetType(WeaponType.blaster);
	}

	void Update () 
	{
		// Pull in information from the Input class
		float xAxis = Input.GetAxis("Horizontal");
		float yAxis = Input.GetAxis("Vertical");
		// Change transform.position based on the axes
		Vector3 pos = transform.position;
		pos.x += xAxis * speed * Time.deltaTime;
		pos.y += yAxis * speed * Time.deltaTime;
		transform.position = pos;
		// Rotate the ship to make it feel more dynamic 
		transform.rotation = Quaternion.Euler(yAxis*pitchMult,xAxis*rollMult,0);

		if(Input.GetAxis("Jump") == 1 && fireDelegate != null) {
			fireDelegate();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		Transform rootT = other.gameObject.transform.root;
		GameObject go = rootT.gameObject;

		if (go == lastTriggerGo)
		{
			return;
		}

		lastTriggerGo = go;

		if (go.tag == "Enemy" || go.tag == "ProjectileEnemy")
		{
			shieldLevel--;
			Destroy(go);
		} 
		else if (go.tag == "PowerUp") 
		{
			AbsorbPowerUp(go);
		} 
		else 
		{
			print("Triggered by non-Enemy: " + go.name);
		}
	}

	public void AbsorbPowerUp(GameObject go) 
	{
		AudioManager.instance.PlaySound("COLLECT_PLAYER");
		PowerUp pu = go.GetComponent<PowerUp>();
		switch(pu.type) 
		{
			case WeaponType.shield:
				shieldLevel++;
				break;

			default:
				if(pu.type == weapons[0].type)
				{
					Weapon w = GetEmptyWeaponSlot();
					if(w != null)
					{
						w.SetType(pu.type);
					}
				}
				else 
				{
					ClearWeapons();
					weapons[0].SetType(pu.type);
				}
				break;
		}
		pu.AbsorbedBy(this.gameObject);
	}

	public float shieldLevel {
		get { 
			return(_shieldLevel);
		}
		set {
			_shieldLevel = Mathf.Min(value, 4);
			if(value < 0) {
				AudioManager.instance.PlaySound("EXPLODE_PLAYER");
				Destroy(this.gameObject);
				Main.S.DelayedRestart(gameRestartDelay);
			}
		}
	}

	Weapon GetEmptyWeaponSlot() 
	{
		for(int i = 0; i < weapons.Length; i++)
		{
			if(weapons[i].type == WeaponType.none)
			{
				return(weapons[i]);
			}
		}

		return(null); 
	}

	void ClearWeapons() 
	{
		foreach(Weapon w in weapons)
		{
			w.SetType(WeaponType.none);
		}
	}
}